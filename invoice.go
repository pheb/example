package main

import (
	"bytes"
	"image/color"
	"image/png"
	"io"

	"github.com/jung-kurt/gofpdf"
)

// Invoice represents all of the information on an invoice PDF.
type Invoice struct {
	Metadata        PdfMetadata
	AccentColor     color.RGBA
	Logo            []byte // must be 300dpi .png file!
	DocumentType    string
	CustomerAddress []string
	TopInfo         []LabelValuePair
	LineItems       []LineItem
	Totals          []LabelValuePair
	PaymentInfo     []string
	MessageLine     string
	Footer          string
}

// PdfMetadata represents the document properties stored inside the PDF
type PdfMetadata struct {
	Title    string
	Author   string
	Subject  string
	Keywords string
}

// LabelValuePair represents label/value pairs in TopInfo and Totals.
type LabelValuePair struct {
	Label string
	Value string
}

// LineItem represents a single line item on the invoice.
type LineItem struct {
	Description string
	Quantity    string
	UnitPrice   string
	ExtPrice    string
}

// RenderToBytes returns a rendered PDF invoice as a []byte.
func (inv *Invoice) RenderToBytes() []byte {
	pdf := gofpdf.New("P", "in", "Letter", "")
	pdf.AddPage()

	// Windows code page 1252 is our character set. Unfortunately full Unicode
	// support is not as easy in PDF as it is in Go :(
	cp1252 := pdf.UnicodeTranslatorFromDescriptor("cp1252")

	// This is always going to be a single-page document; turn off page breaks
	// to allow is to write the footer close to the bottom.
	pdf.SetAutoPageBreak(false, 0)

	// Set metadata; users see this in Document Properties of their PDF reader
	// The "false" here means these strings are in our code page, not UTF-8
	pdf.SetTitle(inv.Metadata.Title, false)
	pdf.SetAuthor(inv.Metadata.Author, false)
	pdf.SetSubject(inv.Metadata.Subject, false)
	pdf.SetKeywords(inv.Metadata.Keywords, false)

	// Document type (upper left corner, large text)
	pdf.SetFont("Helvetica", "B", 30)
	pdf.SetXY(0.50, 0.50+30/72)
	pdf.Cell(2, 0.5, inv.DocumentType)

	// Logo. We assume it is 300dpinv.
	logoReader := bytes.NewReader(inv.Logo)
	logoInfo, err := png.DecodeConfig(logoReader)
	if err != nil {
		panic("could not read logo image: " + err.Error())
	}

	logoWidthInches := float64(logoInfo.Width) / 300
	logoReader.Seek(0, io.SeekStart)
	pdf.RegisterImageReader("logo", "PNG", logoReader)
	pdf.Image("logo", 8-logoWidthInches, 0.475, logoWidthInches, 0, false, "", 0, "")

	// Customer address. Coordinates are the baseline of the text, so we put
	// the initial y at 2.125 (the upper-left we desire) plus 11/72 (11 points)
	pdf.SetFont("Helvetica", "", 11)
	y := 2.125 + 11/72
	for _, addrLine := range inv.CustomerAddress {
		pdf.SetXY(0.75, y)
		pdf.CellFormat(3.5, 14/72, cp1252(addrLine), "", 0, "LT", false, 0, "")
		y += 14.0 / 72
	}

	// TopInfo (typically invoice #, date, etc.)
	y = 2.125 + 11/72
	for _, topInfo := range inv.TopInfo {
		pdf.SetXY(5, y)
		pdf.SetFont("Helvetica", "B", 11)
		pdf.CellFormat(1.125, 14/72, cp1252(topInfo.Label+":"), "", 0, "LT", false, 0, "")
		pdf.SetXY(6.25, y)
		pdf.SetFont("Helvetica", "", 11)
		pdf.CellFormat(1.75, 14/72, cp1252(topInfo.Value), "", 0, "LT", false, 0, "")
		y += 14.0 / 72
	}

	// Header rectangle and text for line item header
	pdf.SetFillColor(int(inv.AccentColor.R), int(inv.AccentColor.G), int(inv.AccentColor.B))
	pdf.Rect(0.5, 3.5, 7.5, 0.333, "F")
	pdf.SetFont("Helvetica", "B", 11)
	pdf.SetTextColor(255, 255, 255)
	pdf.SetXY(0.625, 3.5)
	pdf.CellFormat(0.6, 0.333, "Description", "", 0, "LM", false, 0, "")
	pdf.SetXY(5, 3.5)
	pdf.CellFormat(0.75, 0.333, "Quantity", "", 0, "CM", false, 0, "")
	pdf.SetXY(6, 3.5)
	pdf.CellFormat(1, 0.333, "Unit Price", "", 0, "CM", false, 0, "")
	pdf.SetXY(7, 3.5)
	pdf.CellFormat(1, 0.333, "Ext. Price", "", 0, "CM", false, 0, "")

	// Line items
	y = 4
	pdf.SetFont("Helvetica", "", 11)
	pdf.SetTextColor(0, 0, 0)
	for _, lineItem := range inv.LineItems {
		pdf.SetXY(0.625, y)
		pdf.CellFormat(4.25, 0.25, lineItem.Description, "", 0, "LT", false, 0, "")
		pdf.SetXY(5, y)
		pdf.CellFormat(0.75, 0.25, lineItem.Quantity, "", 0, "CT", false, 0, "")
		pdf.SetXY(5.875, y)
		pdf.CellFormat(1, 0.25, lineItem.UnitPrice, "", 0, "RT", false, 0, "")
		pdf.SetXY(6.875, y)
		pdf.CellFormat(1, 0.25, lineItem.ExtPrice, "", 0, "RT", false, 0, "")
		y += 0.25
	}

	// Totals section. Last item is bolded.
	y += 0.5
	for i, total := range inv.Totals {
		if i == len(inv.Totals)-1 {
			pdf.SetFont("Helvetica", "B", 11)
		}
		pdf.SetXY(4.875, y)
		pdf.CellFormat(1.9, 0.25, total.Label+":", "", 0, "RT", false, 0, "")
		pdf.SetXY(6.875, y)
		pdf.CellFormat(1, 0.25, total.Value, "", 0, "RT", false, 0, "")
		y += 0.25
	}

	// Payment-received information.
	y = 9 - .25*float64(len(inv.PaymentInfo))
	pdf.SetFont("Courier", "", 11)
	for _, pmt := range inv.PaymentInfo {
		pdf.SetXY(0.625, y)
		pdf.CellFormat(0.625, 0.25, pmt, "", 0, "LT", false, 0, "")
		y += 0.25
	}

	// Message line. Centered near bottom of document.
	pdf.SetXY(0.50, 9.50)
	pdf.SetFont("Helvetica", "B", 11)
	pdf.SetTextColor(int(inv.AccentColor.R), int(inv.AccentColor.G), int(inv.AccentColor.B))
	pdf.CellFormat(7.5, 0.25, cp1252(inv.MessageLine), "", 0, "CT", false, 0, "")

	// Footer line
	// Gray and centered at bottom of document.
	pdf.SetXY(0.50, 10.25)
	pdf.SetFont("Helvetica", "I", 11)
	pdf.SetTextColor(127, 127, 127)
	pdf.CellFormat(7.5, 0.25, cp1252(inv.Footer), "", 0, "CT", false, 0, "")

	// Render and emit
	temp := bytes.Buffer{}
	pdf.Output(&temp)
	return temp.Bytes()
}
