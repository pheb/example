package main

import (
	"bytes"
	"testing"
)

func TestRenderToBytes(t *testing.T) {
	// borrowing sampleInvoice from main.go! a robust library would include its
	// own test data.
	pdf := sampleInvoice.RenderToBytes()

	if len(pdf) < 4096 || len(pdf) > 5*1024 {
		t.Fatalf("generated PDF is %d bytes, expected between 4k-5k", len(pdf))
	}

	if !bytes.Equal(pdf[0:6], []byte("%PDF-1")) {
		t.Fatalf("generated PDF does not start with PDF header")
	}

	if !bytes.Equal(pdf[len(pdf)-6:], []byte("%%EOF\n")) {
		t.Fatalf("generated PDF does not end with EOF marker")
	}

	// we don't test the contents aside from these sanity checks!
}
