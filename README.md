# Example CodeBuild project (Golang)

This is an example project that goes along with my Bitbucket Cloud to AWS
CodeBuild script, [BB2CodeBuild][bb2cb]. The actual code is a Go program that
creates an example invoice PDF, but the point of this project is less about the
PDF, and more about demonstrating how to create a `buildspec.yml` file against a
project that is slightly more complex than `Hello, world!`, and have the build
run automatically when you push a change to Bitbucket.

## Setting up CodeBuild

If you are using BB2CodeBuild and want to set this up to see it all work:

* Fork this project into your Bitbucket Cloud account.
* Set up a new CodeBuild project named `username-example-master`, assuming that
  you have forked this into `username/example` in your account.
    * For the source, point the CodeBuild project to Bitbucket and your repo.
    * For the build environment, use the most recent AWS-provided Ubuntu Golang
    image. (I tested with 1.7.3.)
    * Set the build specification to **Using buildspec.yml in the source code root
    directory**.
    * Set a location in S3 where the build artifacts should be placed.

At this point, you can test using the **Start build** button in the AWS Console
to start the build, which should complete successfully.

To demonstrate the webhook, configure your Bitbucket repository in the Webhooks
section to point to your installation of BB2CodeBuild, and push up a change. If
you don't know Go, just edit `main.go` and change one of the string constants in
the `sampleInvoice` object.

## Using your build artifacts

When the build runs, it will generate three executable files named `pdfexample`:
one for Windows, Mac, and Linux. You can download these artifacts from the
location you configured in S3, and execute the correct one for your machine's
architecture. You don't need to have Go installed locally to run the executable.

The program itself will display its abbreviated commit hash in the banner (which
comes from the `GIT_COMMIT` environment variable that BB2CodeBuild sent to the
build), and emit a file in the current directory named `invoice.pdf`, which
looks like [this][sample].

```text
PdfExample - v1.0-c2f0795 - built 2017-08-15T00:52:41+0000

Wrote 4455 bytes to 'invoice.pdf'
```

That's all there is to it!

[bb2cb]: https://github.com/peterheb/bb2codebuild
[sample]: https://i.pheb.com/pheb-example-invoice.png
