// This is an example of a program that generates a generic PDF invoice using gofpdf.
// You can thank me when your next startup goes to the moon. But seriously, I am not
// interested in maintaining an invoice-generation library; this is just intended to
// be a standalone example project for testing automated builds. The PDF part is just
// so it does something more complicated than "Hello, world!" and has a third-party
// dependency that must be retrieved by "go get" during the build process.
//
// MIT license. Copyright (c) 2017 Peter Hebert
package main

import (
	"fmt"
	"image/color"
	"io/ioutil"
)

// golint advises against these all-caps names, but I'm just using them so they stand
// out. These are given values during the build with the -ldflags "-X ..." parameter.
var (
	COMMIT_ID  string
	BUILD_DATE string
)

var sampleInvoice = Invoice{
	Metadata: PdfMetadata{
		Title:    "ExampleCorp Invoice",
		Subject:  "Invoice #123456",
		Author:   "ExampleCorp",
		Keywords: "invoice",
	},
	AccentColor:  color.RGBA{31, 78, 121, 255},
	Logo:         getExampleCorpLogoPng(), // 300dpi png file
	DocumentType: "INVOICE",
	CustomerAddress: []string{
		"Janet Nuñez",
		"Acme Widgets, Inc.",
		"123 Main St.",
		"Kansas City, MO  64108",
	},
	TopInfo: []LabelValuePair{
		// Label, Value
		{"Invoice #", "123456"},
		{"Invoice Date", "12-Aug-2017"},
		{"Account #", "9876-5432"},
	},
	LineItems: []LineItem{
		// Description, Quantity, UnitPrice, ExtPrice
		{"Silver Monthly Plan, July 1 - July 31, 2017", "1", "$99.95", "$99.95"},
		{"Premium Support, July 1 - July 31, 2017", "1", "$19.95", "$19.95"},
	},
	Totals: []LabelValuePair{
		// Label, Value
		{"Subtotal", "$119.90"},
		{"Sales tax", "-----"},
		{"TOTAL", "$119.90"},
	},
	PaymentInfo: []string{
		// This section is printed in Courier at the bottom for that after-the-fact look
		"Payment:   $119.90",
		"By:        VISA -6789",
		"Received:  1-Aug-2017",
		"Auth#:     234567",
	},
	MessageLine: "This invoice has been paid in full. Thank you for your business!",
	Footer:      "1024 Unicorn Ave  ·  Palo Alto, CA  94301  ·  (855) 555-5555  ·  support@example.com",
}

func main() {
	fmt.Printf("PdfExample - v1.0-%s - built %s\n\n", COMMIT_ID, BUILD_DATE)
	pdf := sampleInvoice.RenderToBytes()
	ioutil.WriteFile("invoice.pdf", pdf, 0666)
	fmt.Printf("Wrote %d bytes to 'invoice.pdf'\n", len(pdf))
}
